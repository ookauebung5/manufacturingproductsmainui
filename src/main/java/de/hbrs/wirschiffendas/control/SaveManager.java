package de.hbrs.wirschiffendas.control;

import de.hbrs.wirschiffendas.data.entity.Configuration;

import java.io.*;

public class SaveManager {
    private static final String fileName = "savedState.dat";

    public static boolean saveConfigurationToFile(Configuration configuration) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(configuration);

            objectOutputStream.close();
            fileOutputStream.close();

            System.out.println("Successfully saved the state!");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Configuration loadConfigurationFromFile() {
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            Configuration configuration = (Configuration) objectInputStream.readObject();

            objectInputStream.close();
            fileInputStream.close();

            System.out.println("Successfully loaded the state!");
            return configuration;
        } catch (FileNotFoundException f) {
            System.err.println("Keine Datei mit dem Namen \""+ fileName + "\" vorhanden!");
            return null;
        } catch (IOException|ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
