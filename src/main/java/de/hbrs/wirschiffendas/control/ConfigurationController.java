package de.hbrs.wirschiffendas.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.*;
import org.springframework.web.bind.annotation.*;


@RestController
public class ConfigurationController {

    @PostMapping(value = "/statusUpdate", consumes = "application/json")
    public void updateStatus(@RequestBody String statusTransferObject) {
        try {
            StatusTransferItem statusTransferItem = new ObjectMapper().readValue(statusTransferObject, StatusTransferItem.class);
            Status status = statusTransferItem.getStatus();
            AlgorithmIdentifier algorithmIdentifier = statusTransferItem.getAlgorithmIdentifier();
            CommunicationManager.updateStatus(algorithmIdentifier, status);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @PostMapping(value = "/resultUpdate", consumes = "application/json")
    public void updateResult(@RequestBody String resultTransferObject) {
        try {
            ResultTransferItem resultTransferItem = new ObjectMapper().readValue(resultTransferObject, ResultTransferItem.class);
            Result result = resultTransferItem.getResult();
            AlgorithmIdentifier algorithmIdentifier = resultTransferItem.getAlgorithmIdentifier();
            CommunicationManager.updateResult(algorithmIdentifier, result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/status")
    public String getStatus(@RequestParam(value = "id") String id) {
        try {
            AlgorithmIdentifier ai = new ObjectMapper().readValue(id, AlgorithmIdentifier.class);
            return CommunicationManager.getStatus(ai);
        } catch (JsonProcessingException e) {
            System.err.println("Fehler beim ausgeben des Status. Eingabe konnte nicht gemappt werden!");
            e.printStackTrace();
            return "NULL";
        }
    }
}
