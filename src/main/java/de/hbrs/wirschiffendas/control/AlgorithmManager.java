package de.hbrs.wirschiffendas.control;

import de.hbrs.wirschiffendas.data.entity.AlgorithmEntry;
import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;
import de.hbrs.wirschiffendas.service.AlgorithmEntryService;

public class AlgorithmManager {
    public static void setupAlgorithmEntries() {
        AlgorithmEntry liquidAlgorithm = new AlgorithmEntry(AlgorithmIdentifier.LIQUID);
        liquidAlgorithm.setName("Liquid");
        AlgorithmEntry mechanicalAlgorithm = new AlgorithmEntry(AlgorithmIdentifier.MECHANICAL);
        mechanicalAlgorithm.setName("Mechanical");
        AlgorithmEntry softwareAlgorithm = new AlgorithmEntry(AlgorithmIdentifier.SOFTWARE);
        softwareAlgorithm.setName("Software");
        AlgorithmEntryService.addAlgorithmEntries(liquidAlgorithm, mechanicalAlgorithm, softwareAlgorithm);
    }
}
