package de.hbrs.wirschiffendas.control;

import de.hbrs.wirschiffendas.data.entity.*;
import de.hbrs.wirschiffendas.service.AlgorithmEntryService;
import de.hbrs.wirschiffendas.service.ViewUpdateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;


public class CommunicationManager {
    public static void newAnalysis(Configuration config) throws URISyntaxException {
        String createAnalysisURL = "http://localhost:8081/analyse";
        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(createAnalysisURL);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, config,String.class);
    }

    public static void restartAnalysis(Configuration config, AlgorithmIdentifier ae) throws URISyntaxException {
        String createAnalysisURL = String.format("http://localhost:8081/analyse/%s", ae.toString());
        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(createAnalysisURL);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, config,String.class);
    }

    static void updateStatus(AlgorithmIdentifier identifier, Status newStatus) {
        AlgorithmEntry entry = AlgorithmEntryService.getAlgorithmEntryByAlgorithmIdentifier(identifier);
        if (entry == null) {
            System.err.println("Fehler beim aktualisieren des Status. Der Eintrag mit dem Identifier \"" + identifier + "\" konnte nicht gefunden werden.");
            return;
        }
        entry.setStatus(newStatus);
        ViewUpdateService.updateView();
    }

    public static void updateResult(AlgorithmIdentifier identifier, Result result) {
        AlgorithmEntry entry = AlgorithmEntryService.getAlgorithmEntryByAlgorithmIdentifier(identifier);
        if (entry == null) {
            System.err.println("Fehler beim aktualisieren des Results. Der Eintrag mit dem Identifier \"" + identifier + "\" konnte nicht gefunden werden.");
            return;
        }
        entry.setResult(result);
        ViewUpdateService.updateView();
    }

    public static String getStatus(AlgorithmIdentifier ai) {
        AlgorithmEntry ae = AlgorithmEntryService.getAlgorithmEntryByAlgorithmIdentifier(ai);
        if (ae == null) {
            return "NULL";
        }
        return ae.getStatus().toString();
    }
}
