package de.hbrs.wirschiffendas.views;

public interface RefreshableView {
    void refresh();
}
