package de.hbrs.wirschiffendas.views.manufacturingproducts;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.server.VaadinSession;
import de.hbrs.wirschiffendas.control.AlgorithmManager;
import de.hbrs.wirschiffendas.control.CommunicationManager;
import de.hbrs.wirschiffendas.control.SaveManager;
import de.hbrs.wirschiffendas.data.entity.AlgorithmEntry;
import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;
import de.hbrs.wirschiffendas.data.entity.Configuration;
import de.hbrs.wirschiffendas.service.AlgorithmEntryService;
import de.hbrs.wirschiffendas.service.ViewUpdateService;
import de.hbrs.wirschiffendas.views.MainLayout;
import de.hbrs.wirschiffendas.views.RefreshableView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.net.URISyntaxException;

@PageTitle("Manufacturing Products")
@Route(value = "ManufacturingProducts/:runningAlgorithmEntryID?/:action?(edit)", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class ManufacturingProductsView extends Div implements RefreshableView {

    private final Grid<AlgorithmEntry> grid = new Grid<>();
    private final MultiselectComboBox<String> startingSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> auxiliaryPTOComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> oilSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> fuelSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> coolingSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> exhaustSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> mountingSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> engineManagementSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> monitoringControlSystemComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> powerTransimissionComboBox = new MultiselectComboBox<>();
    private final MultiselectComboBox<String> gearboxOptionsComboBox = new MultiselectComboBox<>();

    @Autowired
    public ManufacturingProductsView() {

        addClassNames("manufacturing-products-view");


        addFormLayout();
        Button restartButton = new Button("Restart", c -> {
            AlgorithmIdentifier algorithmIdentifier;
            for (AlgorithmEntry ae : grid.getSelectedItems()) {
                algorithmIdentifier = ae.getAlgorithmIdentifier();
                try {
                    CommunicationManager.restartAnalysis(generateConfiguration(), algorithmIdentifier);
                } catch (URISyntaxException e) {
                    System.err.println("Fehler beim erstellen der URI");
                    Notification.show("Algorithmus konnte nicht gestartet werden");
                    e.printStackTrace();
                }
            }
        });
        add(grid, restartButton);


        grid.setItems(AlgorithmEntryService.getAlgorithmEntriesAsList());
        grid.addColumn(AlgorithmEntry::getName).setHeader("Name");
        grid.addColumn(AlgorithmEntry::getStatus).setHeader("Status");
        grid.addColumn(AlgorithmEntry::getResult).setHeader("Result");
        grid.addColumn(AlgorithmEntry::getStarted).setHeader("Started");
        grid.addColumn(AlgorithmEntry::getEnded).setHeader("Ended");
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        ViewUpdateService.setView(this);
    }

    private void refreshGrid() {
        grid.select(null);
        //grid.setItems(AlgorithmEntryService.getAlgorithmEntriesAsList());
        getUI().ifPresent(ui -> ui.access(() -> {
            AlgorithmEntryService.getAlgorithmEntriesAsList().forEach(i -> grid.getDataProvider().refreshItem(i));
        }));
    }

    private void addFormLayout() {
        VerticalLayout embed = new VerticalLayout();
        FormLayout form = new FormLayout();
        embed.setAlignItems(FlexComponent.Alignment.CENTER);
        embed.add(new Label("Optional Equipment"), form);

        startingSystemComboBox.setLabel("Starting System");
        startingSystemComboBox.setItems("Air starter*");

        auxiliaryPTOComboBox.setLabel("Auxiliary PTO");
        auxiliaryPTOComboBox.setItems("Alternator", "140A or 190A", "28V", "2 pole", "bilgepump", "on-engine PTOs");


        oilSystemComboBox.setLabel("Oil System");
        oilSystemComboBox.setItems("Oil replenishment system", "diverter valve for duplex filter");


        fuelSystemComboBox.setLabel("Fuel System");
        fuelSystemComboBox.setItems("Duplex fuel pre-filter", "diverter valve for fuel filter", "monitoring fuel leakage");


        coolingSystemComboBox.setLabel("Cooling System");
        coolingSystemComboBox.setItems("Coolant preheating system freestanding or engine mounted", "integr. seatwater gearbox piping");


        exhaustSystemComboBox.setLabel("Exhaust System");
        exhaustSystemComboBox.setItems("90° Exhaust bellows discharge rotatable");


        mountingSystemComboBox.setLabel("Mounting System");
        mountingSystemComboBox.setItems("Resilient mounts at driving end");


        engineManagementSystemComboBox.setLabel("Engine Management System");
        engineManagementSystemComboBox.setItems("In compliance with Classification Society Regulations");


        monitoringControlSystemComboBox.setLabel("Monitoring/Control System");
        monitoringControlSystemComboBox.setItems("BlueVision|NewGeneration");


        powerTransimissionComboBox.setLabel("Power Transmission");
        powerTransimissionComboBox.setItems("Torsionally resilient coupling");


        gearboxOptionsComboBox.setLabel("Gearbox Options");
        gearboxOptionsComboBox.setItems("Reverse reduction gearbox", "el. actuated", "gearbox mounts", "trolling mode for dead-slow propulsion", "free auxiliary PTO", "hydraulic pump drives");
        loadConfig();

        // Buttons
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        Button runAnalysis = new Button("Analyse starten");
        runAnalysis.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        runAnalysis.addClickListener(c -> this.handleAddAnalysis());

        Button saveButton = new Button("Save");
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.addClickListener(c -> {
            Configuration configToSave = generateConfiguration();
            SaveManager.saveConfigurationToFile(configToSave);
            Notification.show("Stand gespeichert!");
        });

        Button clearButton = new Button("Clear");
        clearButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        clearButton.addClickListener(c -> form.getChildren().forEach(component -> {
            if (component instanceof MultiselectComboBox<?> box) {
                box.clear();
            }
        }));

        buttonLayout.add(runAnalysis, saveButton, clearButton);

        form.add(startingSystemComboBox, auxiliaryPTOComboBox, oilSystemComboBox, fuelSystemComboBox, coolingSystemComboBox, exhaustSystemComboBox, mountingSystemComboBox, engineManagementSystemComboBox, monitoringControlSystemComboBox, powerTransimissionComboBox, gearboxOptionsComboBox, buttonLayout);
        add(embed);
    }

    private void handleAddAnalysis() {
        Configuration newEntry = generateConfiguration();
        try {
            CommunicationManager.newAnalysis(newEntry);
        } catch (URISyntaxException e) {
            Notification.show("Fehlgeschlagen: URL falsch geformt.");
            e.printStackTrace();
        }
        refresh();
    }

    private Configuration generateConfiguration() {
        Configuration resultConfig = new Configuration();
        resultConfig.setStartingSystemSet(startingSystemComboBox.getSelectedItems());
        resultConfig.setAuxiliaryPTOSet(auxiliaryPTOComboBox.getSelectedItems());
        resultConfig.setCoolingSystemSet(coolingSystemComboBox.getSelectedItems());
        resultConfig.setEngineManagementSystemSet(engineManagementSystemComboBox.getSelectedItems());
        resultConfig.setFuelSystemSet(fuelSystemComboBox.getSelectedItems());
        resultConfig.setMonitoringControlSystemPTOSet(monitoringControlSystemComboBox.getSelectedItems());
        resultConfig.setGearboxOptionsSet(gearboxOptionsComboBox.getSelectedItems());
        resultConfig.setPowerTransimissionPTOSet(powerTransimissionComboBox.getSelectedItems());
        resultConfig.setOilSystemSet(oilSystemComboBox.getSelectedItems());
        resultConfig.setExhaustSystemSet(exhaustSystemComboBox.getSelectedItems());
        resultConfig.setMountingSystemSet(mountingSystemComboBox.getSelectedItems());

        return resultConfig;
    }

    private void loadConfig() {
        Configuration configuration = SaveManager.loadConfigurationFromFile();
        if (configuration==null) {
            System.out.println("Gespeicherte Configuration war leer und wurde ignoriert.");
            return;
        }
        startingSystemComboBox.setValue(configuration.getStartingSystemSet());
        auxiliaryPTOComboBox.setValue(configuration.getAuxiliaryPTOSet());
        coolingSystemComboBox.setValue(configuration.getCoolingSystemSet());
        engineManagementSystemComboBox.setValue(configuration.getEngineManagementSystemSet());
        fuelSystemComboBox.setValue(configuration.getFuelSystemSet());
        monitoringControlSystemComboBox.setValue(configuration.getMonitoringControlSystemPTOSet());
        gearboxOptionsComboBox.setValue(configuration.getGearboxOptionsSet());
        powerTransimissionComboBox.setValue(configuration.getPowerTransimissionPTOSet());
        oilSystemComboBox.setValue(configuration.getOilSystemSet());
        exhaustSystemComboBox.setValue(configuration.getExhaustSystemSet());
        mountingSystemComboBox.setValue(configuration.getMountingSystemSet());
    }

    @Override
    public void refresh() {
        //refreshGrid();
    }
}
