package de.hbrs.wirschiffendas.service;


import de.hbrs.wirschiffendas.data.entity.AlgorithmEntry;
import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class AlgorithmEntryService implements Serializable {
    private static ArrayList<AlgorithmEntry> algorithmEntryList = new ArrayList<>();

    public static boolean addAlgorithmEntry(AlgorithmEntry algorithmEntry) {
        if (entryWithAlgorithmIdentifierAlreadyExists(algorithmEntry.getAlgorithmIdentifier())) {
            System.err.println("AlgorithmEntry wurde nicht hinzugefügt, da es bereits einen Eintrag mit diesem Identifier gab!");
            return false;
        }

        return algorithmEntryList.add(algorithmEntry);
    }

    public static void addAlgorithmEntries(AlgorithmEntry... entries) {
        for (AlgorithmEntry e : entries) {
            addAlgorithmEntry(e);
        }
    }

    public static void setAlgorithmEntries(ArrayList<AlgorithmEntry> algorithmEntries) {
        algorithmEntryList = algorithmEntries;
    }

    public static AlgorithmEntry getAlgorithmEntryByIndex(int index) {
        return algorithmEntryList.get(index);
    }

    public static ArrayList<AlgorithmEntry> getAlgorithmEntriesAsList() {
        return algorithmEntryList;
    }

    public static AlgorithmEntry[] getAlgorithmEntriesAsArray() {
        return algorithmEntryList.toArray(new AlgorithmEntry[0]);
    }

    public static AlgorithmEntry getAlgorithmEntryByAlgorithmIdentifier(AlgorithmIdentifier id) {
        for (AlgorithmEntry ae: algorithmEntryList) {
            if (ae.getAlgorithmIdentifier().equals(id)) {
                return ae;
            }
        }

        return null;
    }

    private static boolean entryWithAlgorithmIdentifierAlreadyExists(AlgorithmIdentifier identifier) {
        return getAlgorithmEntryByAlgorithmIdentifier(identifier) != null;
    }
}
