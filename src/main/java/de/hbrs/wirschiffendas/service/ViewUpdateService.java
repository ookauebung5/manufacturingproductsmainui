package de.hbrs.wirschiffendas.service;

import de.hbrs.wirschiffendas.views.RefreshableView;

public class ViewUpdateService {
    private static RefreshableView view;

    public static void setView(RefreshableView component) {
        view = component;
    }

    public static void updateView() {
        view.refresh();
    }
}
