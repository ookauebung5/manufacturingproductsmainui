package de.hbrs.wirschiffendas.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class AlgorithmEntry {
    private @Id @GeneratedValue Long id;
    private String name;
    private Status status;
    private Result result;
    private LocalDateTime started;
    private LocalDateTime ended;
    private static int counter = 0;
    private AlgorithmIdentifier algorithmIdentifier;

    public AlgorithmEntry(AlgorithmIdentifier algorithmIdentifier) {
        id = (long) ++counter;
        status = Status.NOT_STARTED;
        this.algorithmIdentifier = algorithmIdentifier;
    }

    public AlgorithmEntry() {

    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == Status.RUNNING) {
            this.setStarted(LocalDateTime.now());
        } else if (status == Status.FAILED || status == Status.READY) {
            this.setEnded(LocalDateTime.now());
        }
        this.status = status;
    }

    public LocalDateTime getStarted() {
        return started;
    }

    public void setStarted(LocalDateTime started) {
        this.started = started;
    }

    public LocalDateTime getEnded() {
        return ended;
    }

    public void setEnded(LocalDateTime ended) {
        this.ended = ended;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AlgorithmIdentifier getAlgorithmIdentifier() {
        return algorithmIdentifier;
    }

    public void setAlgorithmIdentifier(AlgorithmIdentifier algorithmIdentifier) {
        this.algorithmIdentifier = algorithmIdentifier;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
