package de.hbrs.wirschiffendas.data.entity;

public enum AlgorithmIdentifier {
    LIQUID,
    MECHANICAL,
    SOFTWARE;

    AlgorithmIdentifier() {

    }
}
