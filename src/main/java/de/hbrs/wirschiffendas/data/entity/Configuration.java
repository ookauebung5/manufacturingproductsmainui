package de.hbrs.wirschiffendas.data.entity;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Random;
import java.util.Set;

@Entity
public class Configuration implements Serializable {
    private Long id;
    private Set<String> startingSystemSet;
    private Set<String> auxiliaryPTOSet;
    private Set<String> oilSystemSet;
    private Set<String> fuelSystemSet;
    private Set<String> coolingSystemSet;
    private Set<String> exhaustSystemSet;
    private Set<String> mountingSystemSet;
    private Set<String> engineManagementSystemSet;
    private Set<String> monitoringControlSystemPTOSet;
    private Set<String> powerTransimissionPTOSet;
    private Set<String> gearboxOptionsSet;

    public Configuration() {
        Random r = new Random();
        id = r.nextLong();
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    @ElementCollection
    public Set<String> getStartingSystemSet() {
        return startingSystemSet;
    }

    public void setStartingSystemSet(Set<String> startingSystemSet) {
        this.startingSystemSet = startingSystemSet;
    }

    @ElementCollection
    public Set<String> getAuxiliaryPTOSet() {
        return auxiliaryPTOSet;
    }

    public void setAuxiliaryPTOSet(Set<String> auxiliaryPTOSet) {
        this.auxiliaryPTOSet = auxiliaryPTOSet;
    }

    @ElementCollection
    public Set<String> getOilSystemSet() {
        return oilSystemSet;
    }

    public void setOilSystemSet(Set<String> oilSystemSet) {
        this.oilSystemSet = oilSystemSet;
    }

    @ElementCollection
    public Set<String> getFuelSystemSet() {
        return fuelSystemSet;
    }

    public void setFuelSystemSet(Set<String> fuelSystemSet) {
        this.fuelSystemSet = fuelSystemSet;
    }

    @ElementCollection
    public Set<String> getCoolingSystemSet() {
        return coolingSystemSet;
    }

    public void setCoolingSystemSet(Set<String> coolingSystemSet) {
        this.coolingSystemSet = coolingSystemSet;
    }

    @ElementCollection
    public Set<String> getExhaustSystemSet() {
        return exhaustSystemSet;
    }

    public void setExhaustSystemSet(Set<String> exhaustSystemSet) {
        this.exhaustSystemSet = exhaustSystemSet;
    }

    @ElementCollection
    public Set<String> getMountingSystemSet() {
        return mountingSystemSet;
    }

    public void setMountingSystemSet(Set<String> mountingSystemSet) {
        this.mountingSystemSet = mountingSystemSet;
    }

    @ElementCollection
    public Set<String> getEngineManagementSystemSet() {
        return engineManagementSystemSet;
    }

    public void setEngineManagementSystemSet(Set<String> engineManagementSystemSet) {
        this.engineManagementSystemSet = engineManagementSystemSet;
    }

    @ElementCollection
    public Set<String> getMonitoringControlSystemPTOSet() {
        return monitoringControlSystemPTOSet;
    }

    public void setMonitoringControlSystemPTOSet(Set<String> monitoringControlSystemPTOSet) {
        this.monitoringControlSystemPTOSet = monitoringControlSystemPTOSet;
    }

    @ElementCollection
    public Set<String> getPowerTransimissionPTOSet() {
        return powerTransimissionPTOSet;
    }

    public void setPowerTransimissionPTOSet(Set<String> powerTransimissionPTOSet) {
        this.powerTransimissionPTOSet = powerTransimissionPTOSet;
    }

    @ElementCollection
    public Set<String> getGearboxOptionsSet() {
        return gearboxOptionsSet;
    }

    public void setGearboxOptionsSet(Set<String> gearboxOptionsSet) {
        this.gearboxOptionsSet = gearboxOptionsSet;
    }
}
