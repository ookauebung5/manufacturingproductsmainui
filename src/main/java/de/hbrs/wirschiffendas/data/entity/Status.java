package de.hbrs.wirschiffendas.data.entity;

import java.io.Serializable;

public enum Status implements Serializable {
    NOT_STARTED ("Not started"),
    RUNNING ("Running"),
    READY ("Ready"),
    FAILED ("Failed");

    private final String stringRepresentation;

    Status(String string ) {
        this.stringRepresentation = string;
    }

    public String toString() {
        return stringRepresentation;
    }
}
