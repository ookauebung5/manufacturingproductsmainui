package de.hbrs.wirschiffendas.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class StatusTransferItem implements Serializable {
    private Status status;
    private AlgorithmIdentifier algorithmIdentifier;
    private Long id;

    public StatusTransferItem (AlgorithmIdentifier algorithmIdentifier, Status status) {
        this.status = status;
        this.algorithmIdentifier = algorithmIdentifier;
    }

    public StatusTransferItem() {

    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public AlgorithmIdentifier getAlgorithmIdentifier() {
        return algorithmIdentifier;
    }

    public void setAlgorithmIdentifier(AlgorithmIdentifier algorithmIdentifier) {
        this.algorithmIdentifier = algorithmIdentifier;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
