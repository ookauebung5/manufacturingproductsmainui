package de.hbrs.wirschiffendas.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class ResultTransferItem implements Serializable {
    private Result result;
    private AlgorithmIdentifier algorithmIdentifier;
    private Long id;

    public ResultTransferItem(Result result, AlgorithmIdentifier algorithmIdentifier) {
        this.result = result;
        this.algorithmIdentifier = algorithmIdentifier;
    }

    public ResultTransferItem() {
        
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public AlgorithmIdentifier getAlgorithmIdentifier() {
        return algorithmIdentifier;
    }

    public void setAlgorithmIdentifier(AlgorithmIdentifier algorithmIdentifier) {
        this.algorithmIdentifier = algorithmIdentifier;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
