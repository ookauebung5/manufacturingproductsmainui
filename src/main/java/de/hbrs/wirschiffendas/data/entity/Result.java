package de.hbrs.wirschiffendas.data.entity;

import java.io.Serializable;

public enum Result implements Serializable {
    FAILED ("Failed"),
    SUCCESS ("Success");

    private final String stringRepresentation;

    Result(String string ) {
        this.stringRepresentation = string;
    }

    public String toString() {
        return stringRepresentation;
    }
}
